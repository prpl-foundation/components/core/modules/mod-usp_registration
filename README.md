# USP Registration module

USP services should register their data models on startup to the USP Broker. Sometimes a proxy is
placed between the USP Broker and the data model providers. The proxy will connect to the USP Broker
via USP/UDS and can be connected to the data model providers via any of the available ambiorix
backends.

An example configuration using ubus and USP is shown below:
```mermaid
graph TD;
  U[USP Broker] <--> P[Proxy Service];
  P <--> B[ubus];
  B <--> S1[Service 1];
  B <--> S2[Service 2];
  B <--> S3[Service 3];
```

The proxy will usually start up quite early in the boot process, because it needs to be ready to
proxy data model queries coming from many different components. On startup, an ambiorix program will
typically register its data model on the available bus connections. In this case the proxy service
will need to register all data models it proxies on the USP connection to the USP Broker. However,
it is important that it doesn't register data models before they are actually reachable by the proxy
itself.

That's what this module is used for. On startup it will check which data models need to be
registered to the USP Broker. For each of these data models, it will check whether it is reachable
on any of the bus connections. If it is reachable, it will register the data model at the USP Broker
and if it is not reachable, it will add the data model to the list of registrations that need to be
retried. If this list is not empty after the initial registrations are done, the module will start a
timer to retry the registration of the failed objects after a short timeout.
