/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>

#include <amxut/amxut_macros.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "usp_registration.h"

#include "test_registration.h"

static amxb_bus_ctx_t* controller_ctx = NULL;
static amxb_bus_ctx_t* agent_ctx = NULL;

enum verify_return_codes {
    verify_return_all_ok,       // Returns OK for all paths
    verify_return_part1_ok,     // Returns OK for first path, but not for second path
    verify_return_part2_ok,     // Returns OK for second path
    verify_return_error,        // Returns nothing and an error code
};

static int test_bool_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    bool actual_value = (bool) value;
    bool expected_value = (bool) check_value_data;

    printf("%s\nExpected value = %d, actual value = %d\n", __func__, expected_value, actual_value);

    assert_int_equal(actual_value, expected_value);
    printf("\n");
    return 1;
}

static int test_variant_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* actual_variant = (amxc_var_t*) value;
    amxc_var_t* expected_variant = (amxc_var_t*) check_value_data;
    int result = 0;

    printf("%s\nExpected variant:\n", __func__);
    fflush(stdout);
    amxc_var_dump(expected_variant, STDOUT_FILENO);
    printf("Actual variant:\n");
    fflush(stdout);
    amxc_var_dump(actual_variant, STDOUT_FILENO);

    assert_int_equal(amxc_var_compare(expected_variant, actual_variant, &result), 0);
    assert_int_equal(result, 0);
    printf("\n");

    amxc_var_delete(&expected_variant);
    return 1;
}

static void test_verify_input_args(bool verify_mapped, amxc_var_t* mappings) {
    check_expected(verify_mapped);
    check_expected(mappings);
}

static amxd_status_t _verify_mock(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  UNUSED amxc_var_t* args,
                                  amxc_var_t* ret) {
    int retval = mock_type(int);

    printf("Verify called with args\n");
    fflush(stdout);
    amxc_var_dump(args, STDOUT_FILENO);

    test_verify_input_args(GET_BOOL(args, "verify_mapped"), GET_ARG(args, "mappings"));

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    switch(retval) {
    case verify_return_all_ok:
        amxc_var_add_key(cstring_t, ret, "Device.MQTT.", "OK");
        amxc_var_add_key(cstring_t, ret, "Device.IP.", "OK");
        retval = 0;
        retval = amxd_status_ok;
        break;
    case verify_return_part1_ok:
        amxc_var_add_key(cstring_t, ret, "Device.MQTT.", "OK");
        amxc_var_add_key(cstring_t, ret, "Device.IP.", "Not responding");
        retval = amxd_status_ok;
        break;
    case verify_return_part2_ok:
        amxc_var_add_key(cstring_t, ret, "Device.IP.", "OK");
        retval = amxd_status_ok;
        break;
    case verify_return_error:
        retval = amxd_status_unknown_error;
    default:
        break;
    }

    return retval;
}

static void test_unlink_from_uri(const char* uri) {
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%s", uri);
    unlink(amxc_string_get(&path, 4));

    amxc_string_clean(&path);
}

static int test_get_retry_time(void) {
    amxc_var_t* usp_section = amxo_parser_get_config(amxut_bus_parser(), "usp");
    return GET_UINT32(usp_section, "verify_retry_timeout");
}

int test_registration_setup(UNUSED void** state) {
    static const char* odl_config = "../common/module_config.odl";
    static const char* odl_proxy = "../common/proxymanager.odl";
    amxc_var_t* usp_section = NULL;
    const char* controller_uri = NULL;
    const char* agent_uri = NULL;

    amxut_bus_setup(state);

    amxut_resolve_function("verify", _verify_mock);
    amxut_dm_load_odl(odl_config);
    amxut_dm_load_odl(odl_proxy);

    usp_section = amxo_parser_get_config(amxut_bus_parser(), "usp");
    controller_uri = GET_CHAR(usp_section, "broker_controller_uri");
    agent_uri = GET_CHAR(usp_section, "broker_agent_uri");

    test_unlink_from_uri(controller_uri);
    test_unlink_from_uri(agent_uri);

    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);
    assert_int_equal(amxb_listen(&controller_ctx, controller_uri), 0);
    assert_int_equal(amxb_listen(&agent_ctx, agent_uri), 0);

    _usp_registration_main(0, amxut_bus_dm(), amxut_bus_parser());

    return 0;
}

int test_registration_teardown(UNUSED void** state) {
    _usp_registration_main(1, amxut_bus_dm(), amxut_bus_parser());
    amxb_free(&controller_ctx);
    amxb_free(&agent_ctx);
    amxut_bus_teardown(state);
    amxb_be_remove_all();

    return 0;
}

void test_can_register_all_at_once(UNUSED void** state) {
    // Starting module will call verify if everything goes well, so we will check that it is called
    // with the expected input arguments
    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, NULL);
    will_return(_verify_mock, verify_return_all_ok);
    _usp_registration_start(NULL, NULL, NULL);

    amxut_bus_handle_events();
}

void test_can_register_in_parts(UNUSED void** state) {
    uint32_t retry_time = test_get_retry_time();
    amxc_var_t* mappings_input = NULL;

    // Assume Device.IP. is not responding after the first verify
    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, NULL);
    will_return(_verify_mock, verify_return_part1_ok);
    _usp_registration_start(NULL, NULL, NULL);
    amxut_bus_handle_events();

    // The verify will be retried for Device.IP.
    amxc_var_new(&mappings_input);
    amxc_var_set_type(mappings_input, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, mappings_input, "Device.IP.", "");

    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, mappings_input);
    will_return(_verify_mock, verify_return_part2_ok);
    amxut_timer_go_to_future_ms(retry_time);
}

void test_verify_can_fail_and_will_retry(UNUSED void** state) {
    uint32_t retry_time = test_get_retry_time();

    // Assume verify fails the first time
    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, NULL);
    will_return(_verify_mock, verify_return_error);
    _usp_registration_start(NULL, NULL, NULL);
    amxut_bus_handle_events();

    // Assume it fails again after a the timer timeout
    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, NULL);
    will_return(_verify_mock, verify_return_error);
    amxut_timer_go_to_future_ms(retry_time);

    // Assume it works now
    expect_check(test_verify_input_args, verify_mapped, test_bool_equal_check, true);
    expect_check(test_verify_input_args, mappings, test_variant_equal_check, NULL);
    will_return(_verify_mock, verify_return_all_ok);
    amxut_timer_go_to_future_ms(retry_time);
}
