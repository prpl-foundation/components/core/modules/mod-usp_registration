MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/$(COMPONENT)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

MOCK_SRC_DIR = ../mocks
MOCK_SOURCES = $(wildcard $(MOCK_SRC_DIR)/*.c)
SOURCES += $(MOCK_SOURCES)

MOCK_WRAP = amxb_register

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
          $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
          -fkeep-inline-functions -fkeep-static-functions \
          -Wno-format-nonliteral -Wno-unused-variable -Wno-unused-but-set-variable \
          $(shell pkg-config --cflags cmocka)

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxo -lamxb \
           -lsahtrace -lamxut

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
