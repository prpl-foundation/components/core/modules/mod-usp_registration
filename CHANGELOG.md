# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.1 - 2024-04-24(09:04:12 +0000)

### Other

- opensource component

## Release v0.1.0 - 2024-04-19(08:30:50 +0000)

### New

- Only register data models to obuspa when they are available on the bus

