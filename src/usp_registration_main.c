/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "usp_registration.h"

#define USP_BE_DEFAULT "/usr/bin/mods/usp/mod-amxb-usp.so"
#define VERIFY_RETRY_TIMEOUT 10000

typedef struct _usp_registration_app {
    amxd_dm_t* dm;                      // The data model
    amxo_parser_t* parser;              // The parser
    amxb_bus_ctx_t* agent_ctx;          // Agent ctx is connected to controller socket and can be used to send agent messages
    amxb_bus_ctx_t* controller_ctx;     // Controller ctx is connected to agent socket and can be used to send controller messages
    amxb_request_t* verify_request;     // To track asynchronous verify request
    amxp_timer_t* verify_retry_timer;   // Timer to retry verify in case not everything was available yet
    amxc_var_t mappings_retry;          // Variant of type hash table to keep track of objects for which we need to retry the verification
} usp_registration_app_t;

static usp_registration_app_t app;

static void usp_registration_connection_read(int fd, UNUSED void* priv) {
    amxb_bus_ctx_t* bus_ctx = (amxb_bus_ctx_t*) priv;
    int rv = 0;

    rv = amxb_read(bus_ctx);
    if(rv != 0) {
        amxp_connection_t* con = amxp_connection_get(fd);
        when_null(con, exit);
        printf("Failed to read from %s (%d), did the remote end hang up?",
               con->uri == NULL ? "accepted" : con->uri, fd);
        printf("=> Removing %s (%d)",
               con->uri == NULL ? "accepted" : con->uri, fd);
        amxp_connection_remove(fd);
        amxb_free(&bus_ctx);
        bus_ctx = NULL;
    }

exit:
    return;
}

static int usp_registration_load_and_connect(void) {
    int retval = -1;
    amxc_var_t* usp_section = amxo_parser_get_config(app.parser, "usp");
    const char* controller_uri = GET_CHAR(usp_section, "broker_controller_uri");
    const char* agent_uri = GET_CHAR(usp_section, "broker_agent_uri");
    const char* be_location = GET_CHAR(usp_section, "backend_location");

    when_str_empty(controller_uri, exit);
    when_str_empty(agent_uri, exit);

    retval = be_location == NULL ? amxb_be_load(USP_BE_DEFAULT) : amxb_be_load(be_location);
    when_failed(retval, exit);

    retval = amxb_set_config(&app.parser->config);
    when_failed(retval, exit);

    // Connections with type AMXP_CONNECTION_BUS are traditionally used to register the data model
    // which is only needed for the agent_ctx (the controller side doesn't have a data model)
    retval = amxb_connect(&app.agent_ctx, controller_uri);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to %s", controller_uri);
        goto exit;
    }
    amxp_connection_add(amxb_get_fd(app.agent_ctx), usp_registration_connection_read,
                        controller_uri, AMXP_CONNECTION_BUS, app.agent_ctx);

    retval = amxb_connect(&app.controller_ctx, agent_uri);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to %s", agent_uri);
        goto exit;
    }
    amxp_connection_add(amxb_get_fd(app.controller_ctx), usp_registration_connection_read,
                        agent_uri, AMXP_CONNECTION_CUSTOM, app.controller_ctx);

exit:
    return retval;
}

/*
   Add everything that is ready to be registered to the registrations list. Add everything that is
   currently unavailable to the mappings_retry table.
   Note that for the mappings_retry table only the key will be used to verify if the path is
   reachable on the bus. The value is unused, so we set it to an empty string.
 */
static void usp_registration_verify_evaluate(amxc_var_t* mappings_retry, amxc_var_t* result) {
    amxc_var_t* usp_section = amxo_parser_get_config(app.parser, "usp");
    amxc_var_t* registrations = GET_ARG(usp_section, "registrations");
    const char* status = amxc_var_constcast(cstring_t, result);
    const char* path = amxc_var_key(result);

    when_null(registrations, exit);
    when_str_empty(status, exit);
    when_str_empty(path, exit);

    if(strcmp(status, "OK") == 0) {
        amxc_var_add(cstring_t, registrations, path);
    } else {
        amxc_var_add_key(cstring_t, mappings_retry, path, "");
    }

exit:
    return;
}

static uint32_t usp_registration_get_retry_timeout(void) {
    amxc_var_t* usp_section = amxo_parser_get_config(app.parser, "usp");
    return GET_UINT32(usp_section, "verify_retry_timeout") == 0 ? VERIFY_RETRY_TIMEOUT : \
           GET_UINT32(usp_section, "verify_retry_timeout");
}

static void usp_registration_verify_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                         amxb_request_t* request,
                                         int status,
                                         UNUSED void* priv) {
    amxc_var_t* usp_section = amxo_parser_get_config(app.parser, "usp");
    amxc_var_t* registrations = GET_ARG(usp_section, "registrations");
    uint32_t retry_timeout = usp_registration_get_retry_timeout();

    if(status != amxd_status_ok) {
        amxp_timer_start(app.verify_retry_timer, retry_timeout);
        goto exit;
    }

    // Reset mappings, and rebuild them based on current verify outputs
    amxc_var_set_type(&app.mappings_retry, AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(result, GETI_ARG(request->result, 0)) {
        usp_registration_verify_evaluate(&app.mappings_retry, result);
    }

    // If there are more things available to register, call amxb_register
    if(amxc_llist_size(amxc_var_constcast(amxc_llist_t, registrations)) > 0) {
        amxb_register(app.agent_ctx, app.dm);
    }

    // If something is not available yet, restart timer for another verify call
    if(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &app.mappings_retry)) > 0) {
        amxp_timer_start(app.verify_retry_timer, retry_timeout);
    }

exit:
    amxb_close_request(&app.verify_request);
    app.verify_request = NULL;
    return;
}

/*
   Check whether the proxied objects are available on the bus before registering them. An optional
   input argument mappings_retry can be provided to call the verify function with a different set
   of mappings. This can be used to only verify the status of the objects that were not yet
   available in a previous call. Checking all of the objects every time causes more overhead.
 */
static int usp_registration_verify(amxc_var_t* mappings_retry) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("ProxyManager.");
    uint32_t retry_timeout = usp_registration_get_retry_timeout();
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "verify_mapped", true);
    if(mappings_retry != NULL) {
        amxc_var_t* mappings = amxc_var_add_key(amxc_htable_t, &args, "mappings", NULL);
        amxc_var_copy(mappings, &app.mappings_retry);
    }

    app.verify_request = amxb_async_call(ctx, "ProxyManager.", "verify", &args, usp_registration_verify_done, NULL);
    if(app.verify_request == NULL) {
        amxp_timer_start(app.verify_retry_timer, retry_timeout);
        goto exit;
    }

    retval = 0;

exit:
    amxc_var_clean(&args);
    return retval;
}

static void usp_registration_verify_retry(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxb_close_request(&app.verify_request);
    app.verify_request = NULL;

    if(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &app.mappings_retry)) > 0) {
        usp_registration_verify(&app.mappings_retry);
    } else {
        usp_registration_verify(NULL);
    }
}

static void usp_registration_app_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    memset(&app, 0, sizeof(usp_registration_app_t));
    app.dm = dm;
    app.parser = parser;
    amxp_timer_new(&app.verify_retry_timer, usp_registration_verify_retry, NULL);
    amxc_var_init(&app.mappings_retry);
}

static void usp_registration_app_clean(void) {
    amxc_var_clean(&app.mappings_retry);
    amxp_timer_delete(&app.verify_retry_timer);
    amxb_close_request(&app.verify_request);
    app.verify_request = NULL;
    app.parser = NULL;
    app.dm = NULL;
}

void _usp_registration_start(UNUSED const char* const sig_name,
                             UNUSED const amxc_var_t* const data,
                             UNUSED void* const priv) {
    int retval = -1;

    retval = usp_registration_load_and_connect();
    when_failed(retval, exit);

    retval = usp_registration_verify(NULL);
    when_failed(retval, exit);

exit:
    return;
}

int _usp_registration_main(int reason,
                           amxd_dm_t* dm,
                           amxo_parser_t* parser) {

    switch(reason) {
    case 0:     // START
        usp_registration_app_init(dm, parser);
        break;
    case 1:     // STOP
        usp_registration_app_clean();
        break;
    }

    return 0;
}
